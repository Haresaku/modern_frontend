const  webpack = require("webpack");

module.exports = {
  entry: {
    app: "./app/entry.js"
  },
  output: {
    filename: 'bundle.js',
    path: __dirname + "/build/"
  },
  module: {
    rules: [
      {
        test: /\.tag$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use:["riot-tag-loader"]
      },
      {
        test: /\.js|\.tag/,
        enforce: 'post',
        exclude: /node_modules/,
        use:["buble-loader"]
      }
    ]
  },
  resolve: {
    extensions: [".js", ".tag"]
  },
  plugins: [
    new webpack.ProvidePlugin({
      riot: "riot"
    })
  ]
}
