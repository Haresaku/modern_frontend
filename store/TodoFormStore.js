module.exports = class TodoFormStore {
  get title() {
    return this._title;
  }
  set title(_title){
    this._title = _title;
  }
  get date() {
    return this._date;
  }
  set date(_date){
    this._date = _date;
  }
  get text() {
    return this._text;
  }
  set text(_text){
    this._text = _text;
  }
  constructor() {
    riot.observable(this);
    this._title = "";
    this._date = "";
    this._text = "";
  }
}