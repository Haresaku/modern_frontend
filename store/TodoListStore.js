module.exports = class TodoListStore {
  get todos() {
    return this._todos;
  }
  set todos(_todos){
    this._todos = _todos;
  }
  constructor() {
    riot.observable(this);
    this._todos = [];
  }

  addTodo(_todo){
    this._todos.push(_todo);
  }

  remove(index){
    this._todos.splice(index,1);
  }
}