<TodoList>
  <div>
    <div class="row" each="{todo, index in this.store.todos}">
      <div class="col l3">{ todo.title }</div>
      <div class="col l3">{ todo.date.toString() }</div>
      <div class="col l5">{ todo.text }</div>
      <div class="col l1 btn grey darken-2" onclick="{click}" index="{index}">×</div>
    </div>
  </div>
  <style>
    todolist {
      height: 85vh
    }
  </style>
  <script>
    this.store = opts.store;
    this.dispatcher = opts.dispatcher;
    this.dispatcher.addStore(this.store);

    this.store.on("post_todo", (action) => {
      this.store.addTodo(action.todo);
      this.update();
    });

    click(e) {
      let index = e.target.attributes.index.value;
      this.store.remove(index);
      this.update()
    }
  </script>
</TodoList>
