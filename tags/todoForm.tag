<TodoForm>
    <form>
      <div class="input-field">
        <input type="text" id="todo_title" onkeyup="{inputting}"/>
        <label for="todo_title">タイトル</label>
      </div>
      <div>
        <label for="todo_date">実行予定日</label>
        <input type="text" id="todo_date" class="datepicker">
      </div>
      <div class="input-field">
        <textarea id="todo_text" class="materialize-textarea" onkeyup="{inputting}"></textarea>
        <label for="todo_text">内容</label>
      </div>
      <a class="waves-effect waves-light btn grey darken-2" onclick="{ click }">追加</a>
    </form>
    <style>
      todoform {
        height: 85vh;
      }
    </style>
    <script>
      this.store = opts.store;
      this.dispatcher = opts.dispatcher;
      this.dispatcher.addStore(this.store);

      this.on("mount", () => {
        $("#todo_date").change((e) => {
          this.store.date = new Date(e.target.value);
        });
      });

      inputting(e) {
        switch(e.target.id) {
          case "todo_title": {
            this.store.title = e.target.value;
          }
          break;
          case "todo_text": {
            this.store.text = e.target.value;
          }
          break;
        }
      }

      click(_e) {
        // クリックしたときにDispatcherへアクションを送信する
        this.dispatcher.trigger("post_todo", {
          todo:{
            title: this.store.title,
            date: this.store.date,
            text: this.store.text
          }
        })
      };
    </script>
</TodoForm>