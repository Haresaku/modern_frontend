<Navbar>
  <nav>
    <div class="nav-wrapper grey darken-2 z-depth-2">
      <a href="#" class="brand-logo">{opts.title}</a>
    </div>
  </nav>
</Navbar>
