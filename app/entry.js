require('../tags/Navbar');
require('../tags/TodoList');
require('../tags/TodoForm');

var TodoListStore = require("../store/TodoListStore");
var TodoFormStore = require("../store/TodoFormStore");
var Dispatcher = require("./dispatcher");

$(document).ready(() => {
  $(".datepicker").pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
});

var dispatcher = new Dispatcher();
riot.mount('Navbar');
riot.mount('TodoForm', {store: new TodoFormStore(), dispatcher: dispatcher});
riot.mount('TodoList', {store: new TodoListStore(), dispatcher: dispatcher});
